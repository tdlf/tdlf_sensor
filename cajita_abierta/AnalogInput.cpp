#include "AnalogInput.h"

int sensorAValue; // Pour ensuite assigner la valeur de valueA à la variable globale
int sensorBValue; // Pour ensuite assigner la valeur de valueB à la variable globale
int sensorCValue;
int sensorDValue; 

namespace {
    BoolParam valueAEnable;
    BoolParam valueBEnable;
    BoolParam valueCEnable;
    BoolParam valueDEnable;
    
    IntParam valueA;
    IntParam valueB;
    IntParam valueC;
    IntParam valueD;
}

void setupAnalogInput(ParamCollector * _pc){

    valueAEnable.set("/analogin/a/enable", 1); // (true, false, true) bool?
    valueAEnable.saveType = SAVE_ON_REQUEST;
    valueBEnable.set("/analogin/b/enable", 1); // (true, false, true) bool?
    valueBEnable.saveType = SAVE_ON_REQUEST;
    valueCEnable.set("/analogin/c/enable", 1); // (true, false, true) bool?
    valueCEnable.saveType = SAVE_ON_REQUEST;
    valueDEnable.set("/analogin/d/enable", 1); // (true, false, true) bool?
    valueDEnable.saveType = SAVE_ON_REQUEST;
    
    valueA.set("/analogin/a", 0.0, 4095, 2048); 
    valueB.set("/analogin/b", 0.0, 4095, 2048);
    valueC.set("/analogin/c", 0.0, 4095, 2048);
    valueD.set("/analogin/d", 0.0, 4095, 2048);

    _pc->add(&valueAEnable);
    _pc->add(&valueA);
    _pc->add(&valueBEnable);
    _pc->add(&valueB);
    _pc->add(&valueCEnable);
    _pc->add(&valueC);
    _pc->add(&valueDEnable);
    _pc->add(&valueD); 
}

void updateAnalogInput(){
    if (valueAEnable.v){
        sensorAValue = analogRead(34);
        valueA.v = sensorAValue;
        Serial.printf("analog A: %04i\n", sensorAValue);
        }
    if (valueBEnable.v){
        sensorBValue = analogRead(33);
        valueB.v = sensorBValue;
        // Serial.printf("analog B: %04i\n", sensorBValue);
        }
    // if (valueCEnable.v){
    //     Serial.printf("analog C: %04i\n", analogRead(34));
    //     }
    // if (valueDEnable.v){
    //     Serial.printf("analog D: %04i\n", analogRead(35));
    //     }
}