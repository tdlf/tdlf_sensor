/**
 * ##copyright##
 * See LICENSE.txt
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.05
 * @since     2022-03-23
 */
 
#include "Printer.h"

namespace{
    Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
    int logLineIdx;
    int logCharIdx;
    char logBuffer[LOG_LINE_ROW_COUNT][MAX_LOG_LINE_WIDTH];
}

size_t webLogIdx;
char webLog[WEB_LOG_MAX_LENGTH];

Printer printer;

Printer::Printer(){}


void Printer::begin(){
    // if(useOled){
    useOled = true;
    useSerial = true;
    useWebLog = true;
    newData = true;
    
    Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
    logLineIdx = 0;
    logCharIdx = 0;
    webLogIdx = 0;
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
    }
    delay(1000);
    display.clearDisplay();

    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0, 7);
    // Printer static text
    
    update();
    // screenSaverTimeStamp = millis();
}
void Printer::setHeaderA(const char *_header){
    strcpy(headerAText,_header);
    newData = true;
}
void Printer::setHeaderB(const char *_header){
    strcpy(headerBText,_header);
    newData = true;
}

void Printer::flush(){
    update();
}

void Printer::update(){
    if(newData){
        display.clearDisplay();
        display.setTextSize(1);
        // alx : could we have an option to pass the headerText size when calling it?
        // display.setTextColor(WHITE);
        // mxd : les couleurs sont fix, screen top -> yellow, bottom white
        display.setCursor(0, 0);
        display.write(headerAText, PRINTER_HEADER_LENGTH);
        display.setCursor(0, 8);
        display.write(headerBText, PRINTER_HEADER_LENGTH);

        display.setTextSize(1);
        for(int i = 0; i < LOG_LINE_ROW_COUNT; i++){
            display.setCursor(0, i*8+8);
            display.write(logBuffer[(i+logLineIdx)%LOG_LINE_ROW_COUNT]);
        }
        display.display(); 
        newData = false;
    }
}

size_t Printer::write(uint8_t _c){
    if(useOled){
        bool newLine = false;
        if(_c == '\n'){
            newLine = true;
        }
        else if(_c == '\r'){
            // skip these
        }
        else {
            logBuffer[logLineIdx][logCharIdx] = _c;
            logCharIdx++;
            if(logCharIdx >= MAX_LOG_LINE_WIDTH){
                newLine = true;
            }
        }
        if(newLine){
            logCharIdx = 0;
            logLineIdx++;
            logLineIdx%= LOG_LINE_ROW_COUNT;
            memset(logBuffer[logLineIdx], 0, MAX_LOG_LINE_WIDTH);
        }
        newData = true;
    }
    if(useSerial) {
        Serial.write(_c);
    }
    if(useWebLog){
        if(webLogIdx < WEB_LOG_MAX_LENGTH){
            webLog[webLogIdx++] = _c;
        }
    }

    return 1;
}

// void Printer::wakePrinter(){
//     // screenSaverTimeStamp = millis();
// }

void Printer::clearWebLog(){
    memset(webLog, 0, WEB_LOG_MAX_LENGTH);
    webLogIdx = 0;
}

size_t Printer::webLogAvailable(){
    return webLogIdx;
}