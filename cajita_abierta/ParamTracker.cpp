#include "ParamTracker.h"
#include "Printer.h"

namespace {
    // Logger loggers[TRACKER_COUNT];
    uint8_t trackerBuffers[TRACKER_COUNT][TRACKER_BUFFER_SIZE];
    size_t trackerWriteIdx;
    size_t trackerReadIdx;
    // addresses for param you wish to track
    BoolParam trackedEnableMidiParam[TRACKER_COUNT];
    BoolParam trackedEnableWebSocketParam[TRACKER_COUNT];
    BoolParam trackedEnableOSCParam[TRACKER_COUNT];
    TextParam trackedParamAddr[TRACKER_COUNT];
    IntParam trackedCCParam[TRACKER_COUNT];
    IntParam trackedChanParam[TRACKER_COUNT];

    // pointers to 
    Param * trackedParamPointers[TRACKER_COUNT];
    
    // record / playback
    BoolParam recordParam;
    BoolParam playParam;
    BangParam rewindParam;
    IntParam playStartParam;
    IntParam playEndParam;
    // saving
    IntParam dataBankParam;
    BangParam saveDataParam;
    BangParam loadDataParam;

    BangParam eraseDataParam;
    const char * filenames[TRACKER_BANK_COUNT] = {
        "/data_1.bin",
        "/data_2.bin",
        "/data_3.bin",
        "/data_4.bin",
        "/data_5.bin",
        "/data_6.bin",
        "/data_7.bin",
        "/data_8.bin",
        "/data_9.bin"
    };
}

void setupParamTracker(ParamCollector *_pc){
    // make sure to add all other parameters we may want to track before
    trackerWriteIdx = 0;
    trackerReadIdx = 0;

    // Add enable midi and enable websocket
    trackedEnableMidiParam[0].set("/tracker/a/enableMidi", 0); // (true, false, true) bool?
    trackedEnableWebSocketParam[0].set("/tracker/a/enableWebSocket", 0); // (true, false, true) bool?
    trackedEnableOSCParam[0].set("/tracker/a/enableOSCParam", 0); // (true, false, true) bool?)

    trackedEnableMidiParam[1].set("/tracker/b/enableMidi", 0); // (true, false, true) bool?
    trackedEnableWebSocketParam[1].set("/tracker/b/enableWebSocket", 0); // (true, false, true) bool?
    trackedEnableOSCParam[1].set("/tracker/b/enableOSCParam", 0);// (true, false, true) bool?
    //trackedEnableMidiParam[2].set("/tracker/c/enableMidi", 0); // (true, false, true) bool?
    //trackedEnableWebSocketParam[2].set("/tracker/c/enableWebSocket", 1); // (true, false, true) bool?
    //trackedEnableMidiParam[3].set("/tracker/d/enableMidi", 0); // (true, false, true) bool?
    //trackedEnableWebSocketParam[3].set("/tracker/d/enableWebSocket", 1); // (true, false, true) bool?
 
    trackedParamAddr[0].set("/tracker/a/param", "none");
    trackedParamAddr[1].set("/tracker/b/param", "none");
    trackedParamAddr[2].set("/tracker/c/param", "none");
    trackedParamAddr[3].set("/tracker/d/param", "none");

    trackedCCParam[0].set("/tracker/a/cc", 0,127,42);
    trackedCCParam[1].set("/tracker/b/cc", 0,127,43);
    trackedCCParam[2].set("/tracker/c/cc", 0,127,44);
    trackedCCParam[3].set("/tracker/d/cc", 0,127,45);

    trackedChanParam[0].set("/tracker/a/chan", 0,127,1);
    trackedChanParam[1].set("/tracker/b/chan", 0,127,1);
    trackedChanParam[2].set("/tracker/c/chan", 0,127,1);
    trackedChanParam[3].set("/tracker/d/chan", 0,127,1);

    for(int i = 0 ; i < TRACKER_COUNT; i++){
        trackedEnableMidiParam[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedEnableMidiParam[i]);
        trackedEnableWebSocketParam[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedEnableWebSocketParam[i]);
        trackedEnableOSCParam[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedEnableOSCParam[i]);
        trackedParamAddr[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedParamAddr[i]);
        trackedParamPointers[i] = nullptr;
        trackedCCParam[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedCCParam[i]);
        trackedChanParam[i].saveType = SAVE_ON_REQUEST;
        _pc->add(&trackedChanParam[i]);
    }
    recordParam.set("/tracker/data/record/go", 0);
    //dont save recordParam
    _pc->add(&recordParam);

    playParam.set("/tracker/data/playback/play",0);
    playParam.saveType = SAVE_ON_REQUEST;
    _pc->add(&playParam);

    playStartParam.set("/tracker/data/playback/start", 0, TRACKER_BUFFER_SIZE, 0);
    playStartParam.saveType = SAVE_ON_REQUEST;
    _pc->add(&playStartParam);

    playEndParam.set("/tracker/data/playback/end", 0, TRACKER_BUFFER_SIZE, TRACKER_BUFFER_SIZE);
    playEndParam.saveType = SAVE_ON_REQUEST;
    _pc->add(&playEndParam);

    dataBankParam.set("/tracker/data/file/slot", 1, TRACKER_BANK_COUNT, 1);
    dataBankParam.saveType = SAVE_ON_REQUEST;
    _pc->add(&dataBankParam);

    saveDataParam.set("/tracker/data/file/save");
    _pc->add(&saveDataParam);
    loadDataParam.set("/tracker/data/file/load");
    _pc->add(&loadDataParam);
}

// void initTracker(){

// }
void saveDataToBank(){
    printer.printf("[trk] saving to bank #%i\n", dataBankParam.v);

    File file = LittleFS.open(filenames[dataBankParam.v-1], "w");
    if(!file){
        printer.printf("[trk] failed to open #%i\n", dataBankParam.v);
    }
    else {
        if(file.write(reinterpret_cast<uint8_t *>(trackerBuffers), sizeof(trackerBuffers)) == 0) {
            printer.println("[trk] save failed");
        }
        else {
            printer.println("[trk] data saved");
        }    
        file.close();
    }
}

void loadDataFromBank(){
    printer.printf("[trk] loading from bank #%i\n", dataBankParam.v);
    File file = LittleFS.open(filenames[dataBankParam.v-1], "r");
    if(!file) {
        printer.printf("[trk] failed to open #%i\n", dataBankParam.v);
    }
    else {
        if(file.available()){
            file.read(reinterpret_cast<uint8_t *>(trackerBuffers), sizeof(trackerBuffers));
            printer.printf("[trk] loaded #%i\n", dataBankParam.v);
            file.close();
        }
        else {
            printer.printf("[trk] problem with file #%i\n", dataBankParam.v);
        }
    }
}

void updateParamTracker(){
    for(int i = 0; i < TRACKER_COUNT; i++){
        // first check if the addresses changed, relink tracked param
        if(trackedParamAddr[i].checkFlag()){
            // nullptr means untracked
            trackedParamPointers[i] = nullptr;
            for(int j = 0; j < paramCollector.index; j++) {
                Param* _p = paramCollector.pointers[j];
                // check addresss and grab pointer
                if(_p->match(trackedParamAddr[i].v)) {
                    trackedParamPointers[i] = _p;
                    printer.printf("[trk] %s\n", _p->address);
                }
            }
        }
        // if valid param to track
        if(trackedParamPointers[i] != nullptr){
            Param * _p = trackedParamPointers[i];
            uint8_t value = 0;
            value = _p->getByteValue();
            //Serial.print("value ici : ");
            //Serial.println(value);
            bool hasChanged = _p->hasChanged();
            if(hasChanged){
                printer.printf("%s -> %03i \n", _p->address, value);
                // printer.printf("{\"message\":{\"%s\":%i}}\n",_p->address, value);
            }
            // save value if recording
            if(recordParam.v){
                trackerBuffers[i][trackerWriteIdx] = value;
                // printer.printf("rec %05i -> %03i\n", trackerWriteIdx, value);
            }
            // set value if playing
            if(playParam.v){
                value = trackerBuffers[i][trackerReadIdx];
                _p->setWithByteValue(value);
                // printer.printf("play %05i -> %03i\n",trackerReadIdx, value);
            }
            
            if (trackedEnableMidiParam[i].v){ // send midi via osc      
                uint8_t midi[4];
                midi[0] = trackedChanParam[i].v;
                midi[1] = value/2;
                midi[2] = trackedCCParam[i].v; // breath controller
                midi[3] = 42; // extra
                // later we can check to send midi or param address
                myMicroOscUdp.sendMidi(_p->address, midi); 
                // Serial.println("midi message sent");
            }

            if(trackedEnableWebSocketParam[i].v){
                char messageString[64]; // buffer to write text to
                sprintf(messageString, "{\"message\":{\"%s\":%i}}",_p->address, value);
                websocketSendMessage(messageString);
                // Serial.println("websocket message sent");
            }

            if(trackedEnableOSCParam[i].v){
                uint32_t maValeur;
                maValeur = value; // 0-255 int
                myMicroOscUdp.sendInt(_p->address, maValeur);
            }
        }
    }

    if(recordParam.checkFlag()){
        printer.println(recordParam.v ? "recording start":"recording end");
        // do stuff when record is pressed
        // as in reset array index or start count down.
    }
    
    if(recordParam.v){
        trackerWriteIdx++;
        // if(trackerWriteIdx < playStartParam.v){
        //     trackerWriteIdx = playStartParam.v;
        // }
        if(trackerWriteIdx >= TRACKER_BUFFER_SIZE){
            recordParam.setBoolValue(false);
            trackerWriteIdx = 0;
        }
    }
    if(playParam.v){
        trackerReadIdx++;
        if(trackerReadIdx < playStartParam.v){
            trackerReadIdx = playStartParam.v;
        }
        else if(trackerReadIdx > playEndParam.v || trackerReadIdx > TRACKER_BUFFER_SIZE){
            trackerReadIdx = playStartParam.v;
        }
    }
    if(saveDataParam.checkFlag()){
        saveDataToBank();
    }
    if(loadDataParam.checkFlag()){
        loadDataFromBank();
    }

}