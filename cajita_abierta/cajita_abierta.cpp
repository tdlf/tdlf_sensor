#include "ESParam.h" 
#include "AnalogInput.h"
#include "Accelerometer.h"
#include "ParamTracker.h"
#include "pressure.h"
#include "Printer.h"

// fill in your wifi credentials
//#define DEFAULT_WIFI_SSID "aziz-net"
//#define DEFAULT_WIFI_PSWD "killdash9"

#define LED_PIN BUILTIN_LED
BoolParam statusLedParam;
IntParam pollingRateParam;
FloatParam testPotParam;

void ledCallback(bool i){
    digitalWrite(LED_PIN, i);
    printer.println(i?"[pr] led on": "[pr] led off");
}
void updateOledHeader(){
    sprintf(printer.headerAText, "%s-%s", getDeviceName(), getDeviceSSID());
    sprintf(printer.headerBText, "%s", getDeviceIp());
}
void setup(){
    // turn LED on at start of boot
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, HIGH);
    Serial.begin(9600);
    printer.begin();

    // set parameters
    statusLedParam.set("/fun/led", 0);
    statusLedParam.setCallback(ledCallback);
    paramCollector.add(&statusLedParam);
    pollingRateParam.set("/sensors/rate", 10,10000,200);
    pollingRateParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&pollingRateParam);
    setupAnalogInput(&paramCollector);
    setupAccelerometerInput(&paramCollector);
    setupPressureSensorInput(&paramCollector);

    testPotParam.set("/input/test/pot/a", 0.2, 1.2, 0.5);
    paramCollector.add(&testPotParam);
    // after adding all sensors setup the tracker
    setupParamTracker(&paramCollector);
    // start param backend
    setupEsparam(&printer);
    startNetwork();
    // set captive portal
    // setupCaptivePortal();

    printer.println("[boot] done booting");

    updateOledHeader();

    digitalWrite(LED_PIN, LOW);
}

unsigned long stamper = 0;
unsigned long sixty = 0;
unsigned long inc  = 0;
unsigned long lastReading;


void loop(){
    // dnsServer.processNextRequest(); // for processing the captive portal requests
    updateEsparam();
    // update the oled screen
    printer.update();
    // update weblog
    if(printer.webLogAvailable() > 0){
        sendToWeblog(webLog);
        printer.clearWebLog();
    }
    
    if (millis() > pollingRateParam.v+lastReading){ // /sensors/pollingRate
        // update sensors at polling rate
        lastReading = millis();
        //updateAccelerometerInput(); 
        updateAnalogInput();
        //testPotParam.setFloatValue(analogRead(33)/3600.);
        updateParamTracker();
    }
    // two lines to print to yellow header on oled

}

