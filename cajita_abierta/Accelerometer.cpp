#include "Accelerometer.h"
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

int accelEnable;
int accelSmooth;

int accelXValue; int accelXChan; int accelXCC; 
int accelYValue; int accelYChan; int accelYCC; 
int accelZValue; int accelZChan; int accelZCC; 
int panValue; int panChan; int panCC; 
int tiltValue; int tiltChan; int tiltCC; 
int rollValue; int rollChan; int rollCC; 

namespace { // pour setter la valeur du capteur à travers l'interface
    BoolParam valueAccelEnable;
    BoolParam valueAccelSmooth;

    IntParam valueX; // Peut-être utile pour 'send on change' ou pour tester
    IntParam valueY;
    IntParam valueZ;
    FloatParam valuePan;
    FloatParam valueTilt;
    FloatParam valueRoll;
}

void setupAccelerometerInput(ParamCollector * _pc){
    valueAccelEnable.set("/accel/enable", 1); // (true, false, true) bool?
    valueAccelEnable.saveType = SAVE_ON_REQUEST;
    valueAccelSmooth.set("/accel/smooth", 1); // 
    valueAccelSmooth.saveType = SAVE_ON_REQUEST;
    _pc->add(&valueAccelEnable);
    _pc->add(&valueAccelSmooth);

    valueX.set("/accel/x/value", 0, 127, 63); // min,max,default value
    valueY.set("/accel/y/value", 0, 127, 63);
    valueZ.set("/accel/z/value", 0.0, 1.0, 0.5);
    valuePan.set("/accel/pan/value", 0.0, 1.0, 0.5);
    valueTilt.set("/accel/tilt/value", 0.0, 1.0, 0.5);
    valueRoll.set("/accel/roll/value", 0.0, 1.0, 0.5);

    _pc->add(&valueX);
    _pc->add(&valueY);
    _pc->add(&valueZ);
    _pc->add(&valuePan);
    _pc->add(&valueTilt);
    _pc->add(&valueRoll);
    // initialize sensor here
     /* Initialise the sensor */
  if(!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
    // Continue and 'unset accelerometer' // Set a flag to not read the accelerometer?
    // while(1);
  } else { 
        accel.setRange(ADXL345_RANGE_2_G);
        Serial.print  ("Range:         +/- "); 
    
        switch(accel.getRange())
        {
            case ADXL345_RANGE_16_G:
            Serial.print  ("16 "); 
            break;
            case ADXL345_RANGE_8_G:
            Serial.print  ("8 "); 
            break;
            case ADXL345_RANGE_4_G:
            Serial.print  ("4 "); 
            break;
            case ADXL345_RANGE_2_G:
            Serial.print  ("2 "); 
            break;
            default:
        Serial.print  ("?? "); 
        break;
        }  
    Serial.println(" g");  
    Serial.println("accel setted upped");
    Serial.print("Accel data rate : ");
    Serial.println(accel.getDataRate());
    }
}

void updateAccelerometerInput(){
    /* Get a new sensor event */ 
    if (valueAccelEnable.v){
        sensors_event_t event; 
        accel.getEvent(&event);
        /* Display the results (acceleration is measured in m/s^2) */
        //Serial.printf("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
        //Serial.printf("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
        //Serial.printf("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");
        // logger.printf("accel/x :%f\n", event.acceleration.x);
        // logger.printf("accel/y :%f\n", event.acceleration.y);
        // logger.printf("accel/z :%f\n", event.acceleration.z); 
        accelXValue = atan(event.acceleration.y / sqrt(pow(event.acceleration.x, 2) + pow(event.acceleration.z, 2))) * 180 / PI;
        accelXValue = map(accelXValue,-87,88,0,127);
        //Serial.print("accelXValue : ");Serial.println(accelXValue);
        // set IntValue
        valueX.v = accelXValue;
        accelYValue = atan(-1 * event.acceleration.x / sqrt(pow(event.acceleration.y, 2) + pow(event.acceleration.z, 2))) * 180 / PI;
        accelYValue = map(accelYValue,-85,90,127,0);
        valueY.v = accelYValue;
        Serial.printf("valueX.v : %03i, valueY.y : %03i \n",valueX.v, valueY.v);
      
    }
}