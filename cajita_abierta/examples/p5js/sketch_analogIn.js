// Create WebSocket connection.
const socket = new WebSocket('ws://192.168.8.183/webparam'); // Changez à l'adresse IP de votre ESP32
//const socket = new WebSocket('ws://localhost:8080'); // Changez à l'adresse IP de votre ESP32

//const socket = new WebSocket('ws://localhost:8080'); // Changez à l'adresse IP de votre ESP32

let analogInValue = 42;

// Connection opened
socket.addEventListener('open', function (event) {
    //socket.send('Hello Server!');
});



// Listen for messages
socket.addEventListener('message', function (event) {
  // console.log('Message from server ', event.data); 
  //let jsonString = event.data;// Your JSON string
  //let parsedData = JSON.parse(jsonString); // Parse the JSON string
  //console.log(parsedData);
  /* if (parsedData.message){ // 
    analogInValue = parsedData.message['/analogin/a'];
    //console.log("Value of /analogin/a:", analogInValue);
    }  */
  const inputString = event.data;
  const regex = /\/analogin\/a -> (\d+)/;
  const match = inputString.match(regex);

  if (match) {
    const extractedValue = match[1];
    analogInValue = int(extractedValue);
    //console.log(int(extractedValue)); // Output: 063
    } else {
    console.log('No match found');
    }  
});

function setup() {
  createCanvas(400, 400);
}

function draw() {
  //background(220);
 
  background(analogInValue);
  fill(255);
  text(analogInValue,100,100);
}