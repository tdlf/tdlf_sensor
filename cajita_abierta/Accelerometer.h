#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

#include "ESParam.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

// Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345); // returns a 'first defined here error'
extern int accelXValue; // pour des variables globales // ... ou float? 
extern int accelYValue;
extern int accelZValue;
extern int panValue;
extern int tiltValue;
extern int rollValue;
// extern bool doubleTap; // NIY

// place all parameter setup in here
void setupAccelerometerInput(ParamCollector * _pc);

void updateAccelerometerInput();


#endif